# SPA (Simple Plugin API)

SPA (Simple Plugin API) is an extensible API to implement all kinds of
plugins. It is inspired by many other plugin APIs, mostly LV2 and
GStreamer.

* SPA [Design](design.md)
* [Data format](pod.md)
* SPA [Buffers](buffer.md)
